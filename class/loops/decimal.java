import java.util.Scanner;

class decimal{

  public static int pow(int n) {
    if(n==0){
      return 1;
    }
    int product = 1;
    for(int i=0;i<n;i++){
      product = product * 2;
    }
    return product;
  }


  public static void main(String[] args) {
    Scanner kb=new Scanner(System.in);
    int no = kb.nextInt();
    int sum=0;
    for(int i=0;no!=0;i++){
      int temp=no%10;
      if(temp==1){
        sum=sum+pow(i);
      }
      no/=10;
    }
    System.out.println(sum);
  }
}