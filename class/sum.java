import java.util.Scanner;

class sum{
  public static void main(String[] args) {
    Scanner kb=new Scanner(System.in);
    int no = kb.nextInt();
    int cases = kb.nextInt();
    int ans =0;
    if(!(no>=1 && (cases==1||cases==2)) ){
      System.out.println(-1);
    }
    else{
      if(cases==1){
        ans=0;
        for(int i=1;i<=no;i++){
          ans=ans+i;
        }
        System.out.println(ans);
      }
      if(cases==2){
        ans=1;
        for(int i=1;i<=no;i++){
          ans=ans*i;
        }
        System.out.println(ans);
      }
    }
  }
}