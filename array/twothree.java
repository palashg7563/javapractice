import java.util.Scanner;

class twothree{
  public static void main(String args[]){
    Scanner kb=new Scanner(System.in);
    int a = kb.nextInt();
    int[] array = new int[a];
    for(int i=0;i<array.length;i++){
      array[i]=kb.nextInt();
    }
    int sum=0;
    for(int i=0;i<array.length;i++){
      if(array[i]%2==0||array[i]%3==0){
        sum=sum+array[i];
      }
    }
    System.out.print(sum);
  }
}